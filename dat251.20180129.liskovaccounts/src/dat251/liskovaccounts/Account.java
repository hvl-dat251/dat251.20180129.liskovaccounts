package dat251.liskovaccounts;

public class Account {
	
	protected String accountNumber;
	protected double balance;
	
	public Account(String accountNumber) {
		this(accountNumber, 0.0);
	}

	public Account(String accountNumber, double balance) {
		this.accountNumber = accountNumber;
		this.balance = balance;
	}
	
	public String getAccountNumber() {
		return accountNumber;
	}
	
	public double getBalance() {
		return balance;
	}

	public void deposit(double amount) {
		balance += amount;
	}
	
	/**
	 * Contract:
	 * 		Precondition: amount >= 0
	 * 		Postcondition: balance = balance - amount
	 * @param amount
	 */
	public void withdraw(double amount) {
		assert amount >= 0;
		balance -= amount;
	}
	
	public String toString() {
		return "AccountNo " + accountNumber
			 + ": Balance = " + balance;
	}
}
