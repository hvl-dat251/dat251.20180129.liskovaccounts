package dat251.liskovaccounts;

import java.util.List;

public class TransactionProcessor {
	
	public void process(List<Transaction> transactions) {
		
		for (Transaction t : transactions) {
			
			Account a = AccountDAO.findAccount(t.getAccountNumber());
			
			if (t.getTtype() == TransactionType.Deposit) {
				a.deposit(t.getAmount());
			}
			if (t.getTtype() == TransactionType.Withdraw) {
				a.withdraw(t.getAmount());
			}
		}
	}
	
}
