package dat251.liskovaccounts;

import java.util.ArrayList;
import java.util.List;

public class Main {
	
	public static void main(String[] args) {
		
		TransactionProcessor tp = new TransactionProcessor();
		
		List<Transaction> transactions = new ArrayList<Transaction>();
		
		transactions.add(new Transaction("100", 100, TransactionType.Deposit));
		transactions.add(new Transaction("200", 100, TransactionType.Deposit));
		transactions.add(new Transaction("300", 100, TransactionType.Deposit));
		transactions.add(new Transaction("400", 100, TransactionType.Deposit));
		
		transactions.add(new Transaction("100", 20, TransactionType.Withdraw));
		transactions.add(new Transaction("200", 40, TransactionType.Withdraw));
		transactions.add(new Transaction("300", 60, TransactionType.Withdraw));
		transactions.add(new Transaction("400", 120, TransactionType.Withdraw));

//		transactions.add(new Transaction("100", 100, TransactionType.Withdraw));
//		transactions.add(new Transaction("200", 80, TransactionType.Withdraw));
		
		tp.process(transactions);
		
		System.out.println(AccountDAO.getAccounts().values());
	}

}
