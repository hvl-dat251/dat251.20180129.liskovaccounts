package dat251.liskovaccounts;

@SuppressWarnings("serial")
public class InvalidTransactionException extends RuntimeException {

	public InvalidTransactionException(String message) {
		super(message);
	}
}
