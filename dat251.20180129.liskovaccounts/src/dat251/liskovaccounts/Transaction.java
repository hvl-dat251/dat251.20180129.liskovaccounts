package dat251.liskovaccounts;

public class Transaction {

	private String accountNumber;
	private TransactionType transactionType;
	private double amount;

	public Transaction(String accountNumber, double amount,
			TransactionType transactionType) {
		
		this.accountNumber = accountNumber;
		this.amount = amount;
		this.transactionType = transactionType;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public TransactionType getTtype() {
		return transactionType;
	}

	public double getAmount() {
		return amount;
	}
}
