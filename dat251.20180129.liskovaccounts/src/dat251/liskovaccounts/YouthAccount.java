package dat251.liskovaccounts;

public class YouthAccount extends Account {

	public YouthAccount(String accountNumber) {
		super(accountNumber);
	}

	public YouthAccount(String accountNumber, double balance) {
		super(accountNumber, balance);
	}

	//Liskov violation ... ?
	@Override
	public void withdraw(double amount) {
		
		if (amount > balance) {
			throw new InvalidTransactionException(
					"Not allowed to overdraw a YouthAccount");
		}
		super.withdraw(amount);
	}
}
