package dat251.liskovaccounts;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.Test;


public class MainAsJUnitTest {
	
	@Test
	public void testTransactionProcessing() {
		
		TransactionProcessor tp = new TransactionProcessor();
		
		List<Transaction> transactions = new ArrayList<Transaction>();
		
		transactions.add(new Transaction("100", 100, TransactionType.Deposit));
		transactions.add(new Transaction("200", 100, TransactionType.Deposit));
		transactions.add(new Transaction("300", 100, TransactionType.Deposit));
		transactions.add(new Transaction("400", 100, TransactionType.Deposit));
		
		transactions.add(new Transaction("100", 20, TransactionType.Withdraw));
		transactions.add(new Transaction("200", 40, TransactionType.Withdraw));
		transactions.add(new Transaction("300", 60, TransactionType.Withdraw));
		transactions.add(new Transaction("400", 120, TransactionType.Withdraw));

//		transactions.add(new Transaction("100", 100, TransactionType.Withdraw));
//		transactions.add(new Transaction("200", 80, TransactionType.Withdraw));
		
		
		tp.process(transactions);
		
		Map<String, Account> accounts = AccountDAO.getAccounts();
		
		assertEquals(80, accounts.get("100").getBalance(), 0.0);
		assertEquals(60, accounts.get("200").getBalance(), 0.0);
		assertEquals(40, accounts.get("300").getBalance(), 0.0);
		assertEquals(-20, accounts.get("400").getBalance(), 0.0);
	}

}
