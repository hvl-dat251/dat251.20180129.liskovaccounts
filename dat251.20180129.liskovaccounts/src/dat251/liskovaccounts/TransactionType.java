package dat251.liskovaccounts;

public enum TransactionType {
	Deposit, Withdraw
}
