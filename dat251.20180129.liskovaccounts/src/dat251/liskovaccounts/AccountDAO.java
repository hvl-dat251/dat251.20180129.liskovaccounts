package dat251.liskovaccounts;

import java.util.HashMap;
import java.util.Map;

/**
 * A fake "singleton" implementation of the AccountDAO
 */
public class AccountDAO {

	private static Map<String, Account> accounts;
	
	static {
		accounts = new HashMap<String, Account>();
		accounts.put("100", new Account("100", 0));
		accounts.put("200", new YouthAccount("200", 0));
		accounts.put("300", new YouthAccount("300", 0));
		accounts.put("400", new Account("400", 0));
		accounts.put("500", new Account("500", 0));

	}
	
	public static Map<String, Account> getAccounts() {
		return accounts;
	}

	public static Account findAccount(String accountNumber) {
		return accounts.get(accountNumber);
	}
}
